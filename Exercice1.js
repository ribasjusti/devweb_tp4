/* Fonction pour ajouter un artiste dans la liste */
function ajoutArtiste() {
  // On récupère l'artiste à ajouter dans le formulaire
  var ajout = document.getElementById("nvArtiste").value;
  // On créer un "option" = nouvel élément
  var option = document.createElement("option");
  // On donne le nom de l'artiste au nouvel élément
  option.innerHTML = ajout;
  // On raccroche l'élément à ma liste
  document.getElementById("listeArtiste").appendChild(option);
  // On adapte la taille de la liste
  tailleListe();
}


/* Fonction pour supprimer un artiste dans la liste */
function suppArtiste() {
  var i;
  // On récupère l'artiste à supprimer dans le formulaire
  var supp = document.getElementById("ancienArtiste").value;
  // On récupère la liste des artistes
  var liste = document.getElementById("listeArtiste");
  for(i=0; i<liste.length; i++){
    var option = liste[i];
    if(option.text.toLowerCase() == supp.toLowerCase()){
      liste.removeChild(liste.children[i]);
    }
  }
  // On adapte la taille de la liste
  tailleListe();
}


/* Fonction pour modifier la taille d'une liste */
function tailleListe() {
  // On compte le nombre d'élément
  var taille = document.getElementById("listeArtiste").children.length;
  var liste = document.getElementById("listeArtiste");
  // On donne le nombre d'éléments comptés à la taille de la liste
  liste.setAttribute("size", taille);
}


function ouvrirFenetre(x){
  nom = x.getAttribute("id");
  console.log(nom);
  var myWindow = window.open("", "Artiste", "width=500,height=500");
  myWindow.document.write("<img style = 'width : 500px;' src='img/" + nom + ".jpg'>");
 }
